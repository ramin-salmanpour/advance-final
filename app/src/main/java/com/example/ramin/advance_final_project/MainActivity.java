package com.example.ramin.advance_final_project;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.view.View;

public class MainActivity extends Activity {

    Button btn1,btn2,btn3,btn4,btn5,btn6;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        bind();
        btn1= (Button)findViewById(R.id.btn1);
        btn2= (Button) findViewById(R.id.btn2);
        btn3= (Button) findViewById(R.id.btn3);
        btn4= (Button)findViewById(R.id.btn4);
        btn5= (Button)findViewById(R.id.btn5);
        btn6= (Button)findViewById(R.id.btn6);

       btn1.setOnClickListener(new View.OnClickListener() {

                                              @Override
                                              public void onClick(View V) {

                                                  Intent i = new Intent(MainActivity.this, actv_1.class);
                                                  startActivity(i);
                                              }

                                          }


        );


    }

    ///////////////////////////////////////
    public void runActivity(View V) {
        int id = V.getId();
        Intent i = null;
//        if (id == R.id.btn1)
//            i = new Intent(MainActivity.this, actv_1.class);
//        else
            if (id == R.id.btn2)
            i = new Intent(MainActivity.this, actv_2.class);
        else if (id == R.id.btn3)
            i = new Intent(MainActivity.this,actv_3.class);
        else if (id == R.id.btn4)
            i = new Intent(MainActivity.this, actv_4.class);
        else if (id == R.id.btn5)
            i = new Intent(MainActivity.this, actv_5.class);
        else if (id == R.id.btn6)
            i = new Intent(MainActivity.this, actv_6.class);
        
        startActivity(i);
    }





    //////////////////////////////////////binds
//    private void bind(){
//         btn1= (Button)findViewById(R.id.btn1);
//         btn2= (Button) findViewById(R.id.btn2);
//         btn3= (Button) findViewById(R.id.btn3);
//         btn4= (Button)findViewById(R.id.btn4);
//         btn5= (Button)findViewById(R.id.btn5);
//         btn6= (Button)findViewById(R.id.btn6);
//    }

}
